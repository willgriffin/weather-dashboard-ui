import Vue from 'vue';
// import VueNativeSock from 'vue-native-websocket'
import App from './App.vue';
import store from './store'

// Vue.use(VueNativeSock, process.env.VUE_APP_WEBSOCKETS_URL, {
//   reconnection: true, // (Boolean) whether to reconnect automatically (false)
//   reconnectionAttempts: 5, // (Number) number of reconnection attempts before giving up (Infinity),
//   reconnectionDelay: 3000, // (Number) how long to initially wait before attempting a new (1000)
// })

Vue.config.productionTip = false;

new Vue({
  store,
  render: h => h(App),
}).$mount('#app');
