import Vue from 'vue'
import Vuex, { StoreOptions } from 'vuex'
import axios from 'axios'
import VueAxios from 'vue-axios'
import { RootState } from './types'
import { weather } from './weather/index'

Vue.use(Vuex)
Vue.use(VueAxios, axios)
Vue.axios.defaults.baseURL = process.env.VUE_APP_API_BASE_URL

const store: StoreOptions<RootState> = {
  state: {
    version: '0.0.1',
  },
  modules: {
    weather,
  }
}

export default new Vuex.Store<RootState>(store)