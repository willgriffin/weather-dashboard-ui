import Vue from 'vue'
import { ActionTree } from 'vuex'
import { WeatherState, WeatherCity, WeatherConditions } from './types'
import { RootState } from '../types'

export const actions: ActionTree<WeatherState, RootState> = {
  async fetchData({ commit }): Promise<any> {
    const results = await Vue.axios.get('weather')
    commit('updateCities', results.data)
  }
}