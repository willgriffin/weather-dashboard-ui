import { Module } from 'vuex';
import { getters } from './getters'
import { actions } from './actions'
import { mutations } from './mutations'
import { WeatherState } from './types'
import { RootState } from '../types'

export const state: WeatherState = {
  cities: [],
  error: false
}

const namespaced: boolean = true

export const weather: Module<WeatherState, RootState> = {
  namespaced,
  state,
  getters,
  actions,
  mutations
}