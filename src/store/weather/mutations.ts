import { MutationTree } from 'vuex'
import axios from 'axios'
import { WeatherState, WeatherCity, WeatherConditions } from './types'
import { RootState } from '../types'

export const mutations: MutationTree<WeatherState> = {
  updateCities(state, payload: Array<WeatherCity>) {
    state.error = false
    state.cities = payload
  }
}