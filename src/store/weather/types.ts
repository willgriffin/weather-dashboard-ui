
export interface WeatherConditions {
  type: string;
  title: string;
  link: string;
  updated: string;
  published: string;
  summary: string;
  observedAt?: string;
  condition?: string;
  temperature?: string;
  pressureTendency?: string;
  visibility?: string;
  humidity?: string;
  dewpoint?: string;
  wind?: string;
  airQualityHealthIndex?: string;
  inEffect?: boolean;
}

export interface WeatherCity {
  slug: string;
  code: string;
  lat: number;
  lng: number;
  phone?: string;
  current: WeatherConditions;
}

export interface WeatherState {
  cities?: Array<WeatherCity>;
  error: boolean;
}